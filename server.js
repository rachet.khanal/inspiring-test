const fs = require('fs')
const bodyParser = require('body-parser')
const jsonServer = require('json-server')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const server = jsonServer.create()

const router = jsonServer.router('./db/db.json')

const USER_DB = './db/users.json'
const userdb = JSON.parse(fs.readFileSync(USER_DB, 'UTF-8'))

server.use(bodyParser.urlencoded({extended: true}))
server.use(bodyParser.json())
server.use(jsonServer.defaults({ noCours: true}))


const SECRET_KEY = '123456789'
const expiresIn = '1h'


// Create a token from a payload 
function createToken(payload){
    return jwt.sign(payload, SECRET_KEY, {expiresIn})
}
  
  // Verify the token 
function verifyToken(token){
    return  jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err)
}

//get user index
function getUserIndex({username}){
    return userdb.users.findIndex(user => user.username === username )
}

// Check if the user exists with correct password in database
async function isAuthenticated({username, password}){
    const index = getUserIndex({username});
    if(index === -1) return false
    return await bcrypt.compare(password, userdb.users[index].password);
}

function checkUserEmailExists({ email }){
    return userdb.users.findIndex(user => user.email === email) !== -1
}

function checkUsernameExists({ username }){
    return userdb.users.findIndex(user => user.username === username) !== -1
}

server.post('/auth/register', async (req, res) => {
    console.log('register new user')
    if(checkUserEmailExists(req.body)) return res.status(401).json({"status": 401, "message": "Sorry the email is already registered."}) 
    if(checkUsernameExists(req.body)) return res.status(401).json({"status": 401, "message": "Sorry the user name is already taken."}) 
    let newUser = req.body
    newUser['id'] = userdb.users.length + 1
    const salt = await bcrypt.genSalt(10);
    newUser['password']  = await bcrypt.hash(newUser['password'], salt)
    userdb.users.push(newUser);
    fs.writeFile(USER_DB, JSON.stringify(userdb), (err, data) => {
        if(err){
            console.log(err)
            const status = 500
            message = "An error occurred."
            res.status(status).json({status, message})
        }
        console.log('done registering new user')
        const status = 200
        message = {user: newUser}
        res.status(status).json({status, message})
    })
    return
})

server.get('/auth', async (req, res) => {
    if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
        const status = 401
        const message = 'Bad authorization header'
        res.status(status).json({status, message})
        return
    }
    try {
        verifyToken(req.headers.authorization.split(' ')[1])
        const {username} = jwt.decode(req.headers.authorization.split(' ')[1])
        const index = getUserIndex({username})// needs to get from jwt
        if( index === -1 ){
            throw new Error('Invalid user token.');
        }
        const status = 200
        const message = { "username": userdb.users[index].username, "full_name": userdb.users[index].full_name }
        return res.status(status).json({status, message})
    } catch (err) {
        const status = 401
        const message = 'Error: access_token is not valid'
        return res.status(status).json({status, message})
    }
})


server.post('/auth/login', async (req, res) => {
    const {username, password} = req.body
    const valid = await isAuthenticated({username, password})
    if (!valid) {
      const status = 401
      const message = 'Incorrect username or password'
      res.status(status).json({status, message})
      return
    }
    const access_token = createToken({username})
    res.status(200).json({access_token})
})

server.use(/^(?!\/auth).*$/,  (req, res, next) => {
    if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
        const status = 401
        const message = 'Bad authorization header'
        res.status(status).json({status, message})
        return
    }
    try {
        verifyToken(req.headers.authorization.split(' ')[1])
        next()
    } catch (err) {
        const status = 401
        const message = 'Error: access_token is not valid'
        res.status(status).json({status, message})
    }
})

server.use(router)

server.listen(3333, () => {
    console.log('Run Auth API Server')
})