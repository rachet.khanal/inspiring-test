import React, { useEffect } from 'react';
import './App.css';


import { PrivateRoute, GuestRoute } from './components/Route';
import { publicRoutes, authRoutes, guestRoutes } from './routes/';
import * as actions from "./store/actions"; 

import { Redirect, BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import { useDispatch, useSelector } from 'react-redux';
import Loader from 'react-loader-spinner'
function App() {
  
  const dispatch = useDispatch();
  const token = useSelector((state) => state.auth.token);
  const isAuthenticated = (token) ? true : false ;
  let isLoading = useSelector(state => state.auth.loading);
  const loader = <section className = "loader-section"><Loader type="BallTriangle" color="#c7a41b" height={100} width={100} /></section>;

  useEffect(() => {
    dispatch( actions.checkAuth() );
  }, []);

  

  return (
    <React.Suspense fallback = {loader}>
      <Router>
          <div className="App">
          <Switch>
            {publicRoutes.map(({path, componentName, exact}, key)=> <Route exact={exact}  key={componentName+key} path={path} component={componentName} />)}
            {authRoutes.map(({path, componentName, exact}, key)=> <PrivateRoute exact={exact} isAuthenticated={isAuthenticated}  key={componentName+key} path={path} component={componentName} />)}
            {guestRoutes.map(({path, componentName, exact}, key)=> <GuestRoute exact={exact} isAuthenticated={isAuthenticated}   key={componentName+key} path={path} component={componentName} />)}
            <Redirect to="/404" />
          </Switch>
          </div>
        </Router>
        {(isLoading)? loader : null}
      </React.Suspense>
  );
}

export default App;
