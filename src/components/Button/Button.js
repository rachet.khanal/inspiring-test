import React from 'react';
import './Button.scss';

export default function Button({ type, classList, onSubmit=null, children, disabled = false, styleDef }){

    return (
        <button style={styleDef} className={`btn ${classList}`} type={type} onClick={ (onSubmit) ? (e) => { e.preventDefault(); onSubmit();} : null } disabled={disabled}>
            {children}
        </button>
    );

}