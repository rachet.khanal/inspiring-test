import React from 'react';
import { Redirect } from "react-router-dom";


export const PrivateRoute = ({ component: Component, isAuthenticated=false  , ...rest}) => {
  
    const respond = ( isAuthenticated ) ? <Component {...rest} /> : <Redirect to={'/login'} />;
  
    return respond;
  }