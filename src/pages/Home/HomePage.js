import React, { useEffect } from 'react';
import Button from '../../components/Button';
import './HomePage.scss';
import Card from '../../components/Card';

import * as actions from "../../store/actions"; 

import { useDispatch, useSelector } from 'react-redux';

export default function HomePage() {

    const dispatch = useDispatch();
    const user = useSelector( state => state.auth.user );

    const logOut = () => {
        console.log('Logout');
        dispatch( actions.authLogout() );
    };

    return(
        <div className="container home-container">
            <Card>
            <div className="card-header">
                    <h2>Welcome {user.full_name} ({user.username}).</h2>
                </div>
                <div className="card-body">
                    <Button onSubmit={logOut} classList="btn--large btn--red" type="button" disabled={false}>
                        Logout
                    </Button>
                </div>
            </Card>  
        </div>
       
    );
}