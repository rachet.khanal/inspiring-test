import React, { useState, useEffect } from 'react';
import Button from '../../components/Button';
import validator from 'validator';
import './Login.scss';

import * as actions from "../../store/actions"; 

import { useDispatch, useSelector } from 'react-redux'

export default function LoginForm(){

    const dispatch = useDispatch();
    const loginError = useSelector(state => state.auth.error);
    
    const initialValues = { 
        username:{ 
            label: "Username", 
            elmentType: 'input',
            config:{ type:"username", value: "", placeholder: "username", autoComplete: "false"},
            validation: { 
                required: true, type: "text", minLength: 5, maxLength: 25
            } 
        }, 
        password:{
            label: "Password",
            elmentType: 'input',
            config:{ type:"password", value: "", placeholder: "password", autoComplete: "false" },
            validation: {
                required: true, type: "password", minLength: 8
            }
        }
    };

    const [ values, setValues ] = useState({ ...initialValues });
    const [ errors, setErrors ] = useState({ username:"", password:"", data:"" });

    useEffect(() => {
        if(loginError) setErrors({ ...errors, data: loginError })
    }, [loginError]);


     /**
     * Handle form input change 
     **/
    const handleInputChange = (name, value) => {
        const updatedForm = { ...values};
        const updatedFormElment = { ...updatedForm[name]};
        const updateFormElConfig = { ...updatedFormElment.config};
        updateFormElConfig.value = value;
        updatedFormElment.config = updateFormElConfig;
        updatedForm[name] = updatedFormElment;
        setValues( updatedForm );
        let newErrors = {...errors}
        checkValidity(newErrors,name, updateFormElConfig.value, updatedFormElment.validation);
        setErrors(newErrors);
    }
    
    /**
     * Validate inputs in form 
     **/
    const checkValidity = ( errObj, name, value, rules ) => {
        let error = "";
        error = ( !validator.isAlphanumeric(value.trim()) )? error + `Only combination of number and letters is allowed.`: error;
        if(name === 'username'){
            error = ( !validator.matches(value.trim(), /^(?=.*[0-9])(?=.*[a-z])([a-z0-9]+)$/) )? error + `Must be combination of uncapitalized letters and numbers.`: error;
        }
        if(rules.type === 'password'){
            error = ( !validator.matches(value.trim(), /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])([a-zA-Z0-9]+)$/) )? error + `Must be combination of small and capital letters and numbers.`: error;
        }
        if(rules.required){
            error = (value.trim() === "")? error + "This information is required. ": error ;
        }
        if(rules.minLength){
            error = ( !validator.isLength(value.trim(), { min : rules.minLength }) )? error + `Minimum ${rules.minLength} charcters are required.`: error;
        }
        if(rules.maxLength){
            error = ( !validator.isLength(value.trim(), { max : rules.maxLength }) )? error + `Maximum ${rules.maxLength} charcters are allowed.`: error;
        }
        errObj[name] = error;
        if(error.trim() !== "" ){  
            return true;
        }
        return false;
    }

    /**
     * Check if there are exisiting errors in the form 
     **/
    function hasError(){
        let error = false;
        let newError = {...errors}
        Object.keys(values).map( (elName) => {
            error = checkValidity(newError, elName, values[elName].config.value, values[elName].validation) || error ;
        });
        setErrors(newError);
        return error;
    }

    /**
     *  Handle sumission of form 
     **/
    const submitForm = () => {
        if(hasError()) return;
        dispatch(actions.auth( values.username.config.value, values.password.config.value ));
    }

    
    // Build input elements for form
    const formElm = Object.entries(values).map( (inputElm) => {
        let inputName = inputElm[0];
        let inputOption = inputElm[1];
        return (
                <div className="form__group" key={inputName} className={(errors[inputName] !== "")? 'group has-error': 'group'}>
                   <input name={inputName} id={inputName} className='form__input' {...inputOption.config} 
                    onChange={({target: {name, value}}) => handleInputChange(name, value)} />

                    <label htmlFor={inputName} className="form__label">{inputOption['label']}</label>
                    <div className="error-message">{errors[inputName]}</div>
                </div>
        );
     } );

    return(
        <div className="container">
             <form className="form" id="login">
                {( errors && errors.data !== "")? <div className="has-error"><span className="error-message">{errors.data}</span></div> : ""}
                <h1 className="form__heading">Welcome</h1>
                {formElm}
                <Button classList="btn--large btn--green" type="button" disabled={false} onSubmit={submitForm}>
                    Login
                </Button>
            </form>
        </div>
    );
}