import React from 'react';
import Button from '../../components/Button';
import {Link} from 'react-router-dom';
import Card from '../../components/Card';

export default function NotFound(){
    return(
        <div className="container" style={{ marginTop: '2rem' }}>
            <Card>
                <div className="card-header">
                    <h2 style={{color: '#2ecc71'}}>Sorry the page you are looking is not found!</h2>
                </div>

                <Button classList="btn--large btn--green" type="button" disabled={false} styleDef={{ marginTop: '2em' }}>
                    <Link to={'/'}>Go Back</Link>
                </Button>
            </Card>
        </div>
    );
}