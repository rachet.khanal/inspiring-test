const HOST = 'http://localhost:3333';
export const API_USER = `${HOST}/auth`
export const API_LOGIN = `${HOST}/auth/login`
export const API_REGISTER = `${HOST}/auth/register`
