import React from "react";

const Home = React.lazy(() => import("../pages/Home"));

export const authRoutes = [
    { name: 'Home', exact:true, path: '/', componentName: Home },
];


