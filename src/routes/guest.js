import React from "react";

const Login = React.lazy(() => import("../pages/Login"));

export const guestRoutes = [
    { name: 'Login', exact:false, path: '/login', componentName: Login} ,
];


