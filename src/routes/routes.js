import React from "react";

const NotFound = React.lazy(() => import("../pages/NotFound/"));

export const publicRoutes = [
    { name: 'NotFound', exact:true, path: '/404', componentName: NotFound },
];


