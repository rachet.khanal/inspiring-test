import * as actionTypes from "./actionTypes";
import axios from "axios";
import { API_USER, API_LOGIN } from "../../routes";

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = (token, user) => {
  localStorage.setItem("token", JSON.stringify(token));
  localStorage.setItem("user", JSON.stringify(user));
  return {
    type: actionTypes.AUTH_SUCCESS,
    token: token,
    user: user
  };
};

export const checkAuth = () => {
  return dispatch => {
    if (window.location.pathname == "/logout") {
      return;
    }
    let token = localStorage.getItem("token");
    if (!token) {
      dispatch(authLogout());
    } else {
      token = JSON.parse(token);
      axios.defaults.headers.ACCEPT = "application/json";
      axios.defaults.headers.Authorization = "Bearer " + token.access_token;
      axios
        .get(API_USER)
        .then((response) => {
          dispatch(authSuccess(token, response.data.message));
        })
        .catch((error) => {
          error.response
            ? dispatch(authFail(error.response.data))
            : dispatch(authFail(error));
          dispatch(authLogout());
        });
    }
  };
};

export const authFail = ({message}) => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: message
  };
};

export const authLogout = () => {
  localStorage.clear();
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const auth = (username, password) => {
  return dispatch => {
    dispatch(authStart());
    axios.defaults.headers.ACCEPT = "application/json";
    axios.defaults.headers.Content = "applicaiton/x-www-form-urlencoded";

    axios
      .post(API_LOGIN, {
        username: username,
        password: password
      })
      .then((response) => {
        let token = response.data;

        axios.defaults.headers.Authorization = "Bearer " + token.access_token;

        axios
          .get(API_USER)
          .then((response) => {
            dispatch(authSuccess(token, response.data.message));
          })
          .catch((error) => {
            error.response
              ? dispatch(authFail(error.response.data))
              : dispatch(authFail(error));
          });
      })
      .catch((error) => {
        error.response
          ? dispatch(authFail(error.response.data))
          : dispatch(authFail(error));
      });
  };
};
